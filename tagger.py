import os
from dotenv import load_dotenv
import pandas as pd
import re
import json
import re
load_dotenv()


openai_api_key = os.getenv("OPENAI_API_KEY")
azure_openai_api_key = os.getenv("OPENAI_API_KEY")
azure_openai_endpoint = os.getenv("AZURE_OPENAI_ENDPOINT")
deployment_name = os.getenv("DEPLOYMENT_NAME")
model_name = os.getenv("MODEL_NAME")
openai_api_version = os.getenv("OPENAI_API_VERSION")
openai_api_type = os.getenv("OPENAI_API_TYPE")

emoji_pattern = re.compile("["
        u"\U0001F600-\U0001F64F"  # emoticons
        u"\U0001F300-\U0001F5FF"  # symbols & pictographs
        u"\U0001F680-\U0001F6FF"  # transport & map symbols
        u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                           "]+", flags=re.UNICODE)


from unidecode import unidecode
from langchain_openai import AzureChatOpenAI
from langchain.chat_models import ChatOpenAI
from langchain.chains import create_tagging_chain
from langchain.prompts import ChatPromptTemplate,PromptTemplate

llm = AzureChatOpenAI(
            openai_api_key=openai_api_key,
            deployment_name=deployment_name,
            model_name=model_name,
            openai_api_version=openai_api_version,
            openai_api_type=openai_api_type,
            azure_endpoint=azure_openai_endpoint,
            temperature=0.0,
            max_tokens=800)


schema = {
    "Sin Descripcion": {"type": "string", "description": "Indica si la descripción no está detallada."},
    "cancelaFueraHorario": {"type": "string", "description": "Indica si se cancela una transacción fuera del horario permitido."},
    "Agente no Atendio": {"type": "string", "description": "Indica si un agente no atendió la solicitud del cliente."},
    "cierreDia": {"type": "string", "description": "Indica si se menciona el cierre del día."},
    "NxRegistro": {"type": "string", "description": "Indica si se menciona el registro en Naranja X."},
    "NxProblemasCI": {"type": "string", "description": "Indica si se tienen problemas con la cuenta de Naranja durante un Cash In."},
    "TarjetadeCredito": {"type": "string", "description": "Indica si se menciona la tarjeta de crédito o no."},
    "SegurosNaranja": {"type": "string", "description": "Indica si se mencionan los seguros de Naranja."},
    "ToqueEnviodeLector": {"type": "string", "description": "Indica si se menciona el toque para el envío de un lector."},
    "NxInfoCI": {"type": "string", "description": "Indica si se solicita información sobre la cuenta durante un Cash In."},
    "Detalledecuenta": {"type": "string", "description": "Indica si se solicita el detalle de la cuenta."},
    "Denunciadetarjeta": {"type": "string", "description": "Indica si se realiza la denuncia de una tarjeta."},
    "Bajadecuenta": {"type": "string", "description": "Indica si se solicita la baja de una cuenta."},
    "Gestiondeentrega": {"type": "string", "description": "Indica si se solicita la gestión de la entrega de un producto."},
    "Requisitosysolicituddetarjetas": {"type": "string", "description": "Indica si se solicitan los requisitos y solicitudes de tarjetas."},
    "NxModificacionDatos": {"type": "string", "description": "Indica si se solicita la modificación de datos en Naranja X."},
    "ErrorIngresoApp": {"type": "string", "description": "Indica si se produce un error en el ingreso a la aplicación."},
    "Activaciondetarjeta": {"type": "string", "description": "Indica si se menciona la activación de una tarjeta."},
    "Aumentodelimites": {"type": "string", "description": "Indica si se menciona el aumento de límites de una cuenta o tarjeta."},
    "Saldoapagar": {"type": "string", "description": "Indica si se solicita información sobre un saldo a pagar."},
    "Phishing": {"type": "string", "description": "Indica si se menciona una técnica de phishing o fraude por correo electrónico."},
    "SaldoDisponible": {"type": "string", "description": "Indica si se menciona el saldo disponible en una cuenta."},
    "NoEspecificaConsulta": {"type": "string", "description": "Indica si no se especifica la consulta realizada."},
    "CIbloqueadoporlimite$": {"type": "string", "description": "Indica si se bloquea la cuenta CI por límite de dinero."},
    "NxRecuperoContrasena": {"type": "string", "description": "Indica si se solicita el recupero de contraseña en Naranja X."},
    "Motivoporrechazodecompra": {"type": "string", "description": "Indica el motivo por el cual se rechaza una compra."},
    "ToqueCobros": {"type": "string", "description": "Indica si se menciona un toque para cobros."},
    "Devolucionoanulaciondecompra": {"type": "string", "description": "Indica si se realiza una devolución o anulación de una compra."},
    "NxDesbloqueoCuenta": {"type": "string", "description": "Indica si se solicita el desbloqueo de una cuenta en Naranja X."},
    "TPDevoluciones": {"type": "string", "description": "Indica si se mencionan las devoluciones realizadas."},
    "Estadodeapertura": {"type": "string", "description": "Indica el estado de apertura de una cuenta o tarjeta."},
    "NxOnboarding": {"type": "string", "description": "Indica si se menciona el proceso de onboarding en Naranja X."},
    "NxCOSucursales": {"type": "string", "description": "Indica si se mencionan las sucursales de Naranja X."},
    "FormasYmedioDePago": {"type": "string", "description": "Indica las formas y medios de pago disponibles."},
    "NxBajaCuenta": {"type": "string", "description": "Indica si se solicita la baja de una cuenta en Naranja X."},
    "reclamoEnvioTarjeta": {"type": "string", "description": "Indica si se realiza un reclamo por el envío de una tarjeta."},
    "FuncionamientodeplanZ": {"type": "string", "description": "Indica si se menciona el funcionamiento de un plan Z."},
    "PagoResumenCuenta": {"type": "string", "description": "Indica si se menciona el pago del resumen de cuenta."},
    "NxProblemasCO": {"type": "string", "description": "Indica si se tienen problemas con la cuenta de Naranja X CO."},
    "DesconocimientoDeCompras": {"type": "string", "description": "Indica si se menciona el desconocimiento de compras realizadas."},
    "Prestamospersonales": {"type": "string", "description": "Indica si se mencionan los préstamos personales."},
    "ComprasInternetoExterior": {"type": "string", "description": "Indica si se mencionan compras en internet o en el exterior."},
    "ErrorenfuncionesdeApp": {"type": "string", "description": "Indica si se produce un error en las funciones de la aplicación."},
    "NxDesconocimientoPesos": {"type": "string", "description": "Indica si se menciona el desconocimiento de pesos en una cuenta NX."},
    "ToqueBloqueoCuenta": {"type": "string", "description": "Indica si se menciona un toque para el bloqueo de una cuenta."},
    "ConsultaRDC": {"type": "string", "description": "Indica si se solicita una consulta sobre RDC."},
    "EnvioDeRDC": {"type": "string", "description": "Indica si se realiza el envío de RDC."},
    "PuertasRojasApp": {"type": "string", "description": "Indica si se menciona el acceso a las puertas rojas en la aplicación."},
    "Estudiojuridico": {"type": "string", "description": "Indica si se menciona un estudio jurídico."},
    "BloqueoClave": {"type": "string", "description": "Indica si se menciona el bloqueo de una clave."},
    "CierredeTNyOM": {"type": "string", "description": "Indica si se menciona el cierre de TN y OM."},
    "Procesamientodepago": {"type": "string", "description": "Indica si se menciona el procesamiento de un pago."},
    "Promociones": {"type": "string", "description": "Indica si se mencionan promociones disponibles."},
    "SolicitudoblanqueodeclaveNOL": {"type": "string", "description": "Indica si se solicita el blanqueo de una clave en NOL."},
    "CuentaNaranjaX": {"type": "string", "description": "Indica si se menciona la cuenta Naranja X."},
    "Bajadedebitoautomatico": {"type": "string", "description": "Indica si se solicita la baja de débito automático."},
    "TPErrorTx": {"type": "string", "description": "Indica si se menciona un error en la transacción."},
    "Cobranzas": {"type": "string", "description": "Indica si se mencionan gestiones de cobranza."},
    "Bajadeadicional": {"type": "string", "description": "Indica si se solicita la baja de un adicional."},
    "Informedepago": {"type": "string", "description": "Indica si se solicita el informe de un pago."},
    "ReclamoDistRDC": {"type": "string", "description": "Indica si se realiza un reclamo sobre una distribución de RDC."},
    "NaranjaPosDepsitos": {"type": "string", "description": "Indica si se mencionan depósitos en Naranja Pos."},
    "Toque-Npos": {"type": "string", "description": "Indica si se menciona un toque para Npos."},
    "NxRecargaTransporte": {"type": "string", "description": "Indica si se solicita la recarga de transporte en Naranja X."},
    "TPDesconocimiento": {"type": "string", "description": "Indica si se menciona el desconocimiento de una transacción."},
    "Consultadeadicionales": {"type": "string", "description": "Indica si se realiza una consulta sobre adicionales."},
    "SolicituddePINparacajero": {"type": "string", "description": "Indica si se solicita un PIN para cajero."},
    "NaranjaPosCuenta": {"type": "string", "description": "Indica si se menciona la cuenta Naranja Pos."},
    "DevoluciondetarjetaOM": {"type": "string", "description": "Indica si se realiza una devolución de tarjeta en OM."},
    "ImpuestoPais": {"type": "string", "description": "Indica si se menciona el impuesto país."},
    "NxPagoServicios": {"type": "string", "description": "Indica si se menciona el pago de servicios en Naranja X."},
    "ToqueCuenta": {"type": "string", "description": "Indica si se menciona un toque para la cuenta."},
    "Modificaciondedatos": {"type": "string", "description": "Indica si se solicita la modificación de datos."},
    "Cancelacionesanticipadas": {"type": "string", "description": "Indica si se solicitan cancelaciones anticipadas."},
    "CostosRenovacionRepoPack": {"type": "string", "description": "Indica si se mencionan los costos de renovación del repo pack."},
    "ToqueTx": {"type": "string", "description": "Indica si se menciona un toque para una transacción."},
    "Comerciosadheridos": {"type": "string", "description": "Indica si se mencionan los comercios adheridos."},
    "InformaciongeneraldeApp": {"type": "string", "description": "Indica si se solicita información general sobre la aplicación."},
    "ClienteRetenido": {"type": "string", "description": "Indica si se menciona la retención de un cliente."},
    "Avisoviajealexterior": {"type": "string", "description": "Indica si se solicita un aviso de viaje al exterior."},
    "TPModificacionDatos": {"type": "string", "description": "Indica si se menciona la modificación de datos de una transacción."},
    "ToqueLdpAcreditacion": {"type": "string", "description": "Indica si se menciona un toque para la acreditación."},
    "NxBeneficios": {"type": "string", "description": "Indica si se mencionan los beneficios de Naranja X."},
    "Extraccionencajeroautomatico": {"type": "string", "description": "Indica si se menciona una extracción en cajero automático."},
    "NxInfoCO": {"type": "string", "description": "Indica si se solicita información en CO."},
    "ToqueCambioDatos": {"type": "string", "description": "Indica si se menciona un toque para el cambio de datos."},
    "TCPrestamos": {"type": "string", "description": "Indica si se mencionan los préstamos a TC."},
    "AjenoaNaranja": {"type": "string", "description": "Indica si se menciona algo ajeno a Naranja."},
    "NoValidadoNosis": {"type": "string", "description": "Indica si no se ha validado en Nosis."},
    "Devoluciondeimporte": {"type": "string", "description": "Indica si se realiza una devolución de importe."},
    "TPEnvio": {"type": "string", "description": "Indica si se menciona un envío."},
    "ToqueLdpCobros": {"type": "string", "description": "Indica si se menciona un toque para cobros."},
    "DisconformidadAtencionrecibida": {"type": "string", "description": "Indica si se tiene disconformidad con la atención recibida."},
    "ProcedimientopagoNOL": {"type": "string", "description": "Indica si se menciona el procedimiento de pago en NOL."},
    "Caidadeherramientas": {"type": "string", "description": "Indica si se menciona la caída de herramientas."},
    "Agradecimientoycierredegestion": {"type": "string", "description": "Indica si se agradece y cierra una gestión."},
    "Bajapordefuncion": {"type": "string", "description": "Indica si se solicita la baja por defunción."},
    "NXInfoUSD": {"type": "string", "description": "Indica si se solicita información en USD."},
    "TPInfo": {"type": "string", "description": "Indica si se solicita información en una transacción."},
    "NaranjaPosCobros": {"type": "string", "description": "Indica si se mencionan los cobros en Naranja Pos."},
    "ToqueBajaCuenta": {"type": "string", "description": "Indica si se menciona un toque para la baja de cuenta."},
    "NxCashOutUSD": {"type": "string", "description": "Indica si se realiza un cash out en USD."},
}

#En caso de querer importar las labels de un archivo, descomente las siguientes lineas

#f = open(r"D:\Programming\Proyectos\NaranjaX\Topics\topics.json") 
#topics = json.load(f)

#schema = {}
#for item in topics.keys():
    #schema[item] = topics["properties"][item]


topics = {"properties": schema}
data = pd.read_csv(r'D:\Programming\Proyectos\NaranjaX\Topics\output.csv')
#Creamos la cadena para tags
chain = create_tagging_chain(topics, llm)
chain.llm_kwargs
chain.output_parser
files = {}

#Usamos solo las primersa 10 rows del DF
data = data.iloc[:10, :]
for index,row in data.iterrows():
    try:
        agent = list(row["AGENT_ANSWERS"])
        tags = chain.run(unidecode(emoji_pattern.sub(r'', row["MESSAGES"])))
        keys = list(tags.keys())
        files[row["ID"]] = {"representante-topic":row["DESC_TRAMITE"],"ml-topics-bot":[(str(i).strip()) for i in keys]}
        
        if len(agent) > 0:
            tags = chain.run(unidecode(emoji_pattern.sub(r'', row["AGENT_ANSWERS"])))
            keys = list(tags.keys())
            files[row["ID"]]["ml-topics-agent"] = [(str(i).strip()) for i in keys]
    
    except Exception as e:
        print(f"{index} - {e}")
        pass


json_file_path = "tags.json"

# Save the dictionary as JSON
with open(json_file_path, "w") as json_file:
    json.dump(files, json_file)

