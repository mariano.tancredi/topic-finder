import pandas as pd
import json
file_path = r'Topics\result (1).csv'

data = pd.read_csv(file_path, usecols=[0, 1, 2])
agent_answers_list = []

for index, row in data.iterrows():
    answers=[]
    agent_answers = []
    conversation = json.loads(row["MESSAGES"])
    agent = False
    for msg in conversation:
        if msg['from'] == 'user':
            if not agent:
                answers.append(f"user:{list(msg['content'].values())[0]}")
            else:
                agent_answers.append(f"user:{list(msg['content'].values())[0]}")

        elif msg['from'] == 'bot':
            if msg['content'].get('text',0) != 0:
                answers.append(f"bot: {msg['content']['text']}")
        
        elif msg['from'] == 'agent':
            agent = True
            if msg['content'].get('text',0) != 0:
                agent_answers.append(f"agent: {msg['content']['text']}")

    row["MESSAGES"] = answers
    agent_answers_list.append(agent_answers)

data["AGENT_ANSWERS"] = agent_answers_list
#En caso de querer filtrar un topic especifco
#data = data[data["DESC_TRAMITE"] == "Aumentodelimites"]

data.to_csv('output.csv', index=False)

